
--берем номера из блек листа
WITH phones AS (
    SELECT
        json_extract_scalar(json, '$.userPhone') AS phone
        , min(json_extract_scalar(json, '$.createdAt')) AS calldate
    FROM
        kafka_raw.calltracking_add_to_blacklist
    WHERE
        from_iso8601_timestamp(json_extract_scalar(json, '$.createdAt')) BETWEEN DATE '2020-08-10' AND DATE '2020-08-17'
    GROUP BY
        json_extract_scalar(json, '$.userPhone')
),


--джойним на secondary_calltracking_recognition_results и получаем кол-во звонков до и после бана
calls as(
    SELECT
    sc.sourcephone
    , p.calldate
    , count(*) as cnt
    , SUM(if(cast(sc.calldate as varchar) < p.calldate, 1, 0)) as before_ban
    , SUM(if(cast(sc.calldate as varchar) > p.calldate, 1, 0)) as after_ban
FROM
    phones p LEFT JOIN kafka_parsed.secondary_calltracking_recognition_results sc
    ON p.phone = sc.sourcephone
WHERE
    ptn_dadd between DATE '2020-08-01' AND DATE '2020-08-17'
group by
    sc.sourcephone
    , p.calldate
)

--смотрим, какие разговоры происходили. для этого джойним target_for_lesha на звонки с номеров из блэклиста
   SELECT
        ct.calltrackingid
        , ct.sourcephone
        , ct.incomingtext
        , ct.outgoingtext
        , regexp_extract_all(lower(ct.incomingtext), 'квартир|комнат|посмотрет|объявлен|прода|этаж|улиц|адрес') as regexp_incoming
        , regexp_extract_all(lower(ct.outgoingtext), 'квартир|комнат|посмотрет|объявлен|прода|этаж|улиц|адрес') as regexp_outgoing
     FROM
        --target_for_lesha джойнится на распознавалку КТ
        emironova.target_for_lesha tfl LEFT JOIN kafka_parsed.secondary_calltracking_recognition_results ct
        ON tfl.calltrackingid = cast(ct.calltrackingid as varchar)
    WHERE ct.ptn_dadd >= DATE '2020-08-10'
    AND (is_phone_spam = 'true' and coalesce(is_agency,'false') = 'false')
    --берем только разговоры с номеров, которые попадали в блэк-лист
    AND ct.sourcephone IN (SELECT phone FROM phones)


